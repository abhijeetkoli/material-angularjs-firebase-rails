Rails.application.routes.draw do

  root 'welcome#index'

  get '/add', to: 'add#index', as: 'add'
  get '/about', to: 'about#index', as: 'about'

end
