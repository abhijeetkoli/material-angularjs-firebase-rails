var appControllers = angular.module('appControllers', []);

appControllers.controller('AppController', [ '$scope', '$rootScope', '$location', '$mdToast', '$firebaseArray', function($scope, $rootScope, $location, $mdToast, $firebaseArray){

    $rootScope.isNewUser = true;
    $rootScope.isSignedIn = false;
    $rootScope.signInProgress = false;
    
    $rootScope.fbref = new Firebase("https://<your-app>.firebaseio.com");

    $rootScope.fbref.onAuth(function(authData) {

        if(authData) {

            if($rootScope.signInProgress === false) {
                
                $rootScope.isSignedIn = true;

                $rootScope.userAvatar = authData.facebook.profileImageURL;
                $rootScope.userName = authData.facebook.displayName;
                $rootScope.userRef = $rootScope.fbref.child("users").child(authData.uid);

            }

        }

    });

    $scope.signIn = function() {

        $rootScope.signInProgress = true;

        $rootScope.fbref.authWithOAuthPopup("facebook", function(error, authData) {
            if (error) {
                if (error.code === "TRANSPORT_UNAVAILABLE") {          
                    //ref.authWithOAuthRedirect("facebook", function(error) {  
                    //});
              }
              $rootScope.signInProgress = false;
          } else if (authData) {

            $rootScope.isSignedIn = true;

            $rootScope.userAvatar = authData.facebook.profileImageURL;
            $rootScope.userName = authData.facebook.displayName;
            $rootScope.userRef = $rootScope.fbref.child("users").child(authData.uid);

            $rootScope.$apply();

            //get user
            $rootScope.userRef.once('value', function (dataSnapshot) {

                $rootScope.userKey = dataSnapshot.key()

                if(dataSnapshot.val()) {
                    $rootScope.user = dataSnapshot.val();
                    $rootScope.isNewUser = false;                                            
                    $rootScope.$apply();
                } else {
                    $rootScope.createUser(authData);
                }

            }, function (error) {                    
                $rootScope.toast(error)
            });

            $rootScope.signInProgress = false;

            }

        });

    };

    $scope.signOut = function() {   
        $rootScope.fbref.unauth();

        $rootScope.isSignedIn = false;

        $rootScope.userAvatar = '';
        $rootScope.userName = '';

        $location.path('/');
    };

    function getName(authData) {
        switch(authData.provider) {
            case 'password':
            return authData.password.email.replace(/@.*/, '');
            case 'twitter':
            return authData.twitter.displayName;
            case 'facebook':
            return authData.facebook.displayName;
        }
    }

    function showAuthError(error) {

        switch (error.code) {
          case "INVALID_EMAIL":
          $rootScope.toast("The specified user account email is invalid.");
          break;
          case "INVALID_PASSWORD":
          $rootScope.toast("The specified user account password is incorrect.");
          break;
          case "INVALID_USER":
          $rootScope.toast("The specified user account does not exist.");
          break;
          default:
          $rootScope.toast("Error logging user in:", error);
      }
    }

    $rootScope.createUser = function(authData) {

        $rootScope.fbref.child("users").child(authData.uid).set({
            provider: authData.provider,
            name: getName(authData),                        
        }, function(error) {

            if (error) {
                $rootScope.toast('Synchronization failed');
            }

        }); 
    }

    $scope.showAdd = function() {
        $location.path('/add');
    };

    $scope.showAbout = function() {
        $location.path('/about'); 
    };

    $rootScope.toast = function(message) {
        $mdToast.show(
          $mdToast.simple()
            .content(message)
            .position({bottom: false, top: true, left: false, right: true})
            .hideDelay(3000)
        );
    };

}]);
