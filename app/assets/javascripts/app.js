var app = angular.module('app', ['ngMaterial', 'ngRoute', 'ngAnimate', 'appControllers', 'firebase']);

app.config(['$httpProvider', function($httpProvider) {
    $httpProvider.defaults.headers.common['X-CSRF-Token'] = $('meta[name=csrf-token]').attr('content');
}]);

app.config(['$mdThemingProvider', function($mdThemingProvider) {
  $mdThemingProvider.theme('default')
    .primaryPalette('indigo')
    .accentPalette('light-blue');
}]);

app.config(['$routeProvider',
  function($routeProvider) {
    $routeProvider.
      when('/add', {
        templateUrl: '/add',
      }).
      when('/about', {
        templateUrl: '/about',
      }).
      otherwise({
        redirectTo: '/'
      });
  }]);
