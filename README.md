# Firebase Rails Seed Project

### Rails
Get Ruby on Rails for [Mac](https://gorails.com/setup/osx/10.10-yosemite) or [Ubuntu](https://gorails.com/setup/ubuntu/15.04). If you are new to rails there are [guides](http://guides.rubyonrails.org/) to help you get started.

### Clone source code
```sh
$ git clone https://abhijeetkoli@bitbucket.org/abhijeetkoli/material-angularjs-firebase-rails.git
```

### Heroku 
Project is configured for heroku deployment. Follow heroku documentation to [create app](https://devcenter.heroku.com/articles/getting-started-with-rails4) and to use [custom domain](https://devcenter.heroku.com/articles/custom-domains).

### Firebase
Create a [Firebase App](https://www.firebase.com/docs/web/quickstart.html) and replace app url in rails project at app/assets/javascripts/controllers.js. Complete [Facebook Authentication](https://www.firebase.com/docs/web/guide/login/facebook.html) steps. 

### Material Angular, AngularJS and AngularFire
Project's UI is developed using [Material AngularJS](https://material.angularjs.org/latest/) and [AngularFire](https://www.firebase.com/docs/web/libraries/angular/guide/), AngularJS binding for Firebase.